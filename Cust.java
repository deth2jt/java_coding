
/**
 * Write a description of class Cust here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
import java.util.Scanner;

public class Cust
{
    // instance variables - replace the example below with your own
    static VendingMachine vm = new VendingMachine();

    protected static enum Money {
        penny,
        nickel,
        dime,
        quarter,
        FUNF,
        EINEDOLL,
        ZWEIDOLL
    }
    /**
     * Constructor for objects of class Cust
     */
    public static void main(String[] a)
    {
        for(int x = 0; x< 50; x++)
            deposit(Money.ZWEIDOLL);
        System.out.println(vm.toString());
        selectItem();
        
    }

    public static void deposit(Money money)
    {
        if(money == Money.penny)
            vm.deposit(.01);
        else if(money == Money.nickel)
            vm.deposit(.05);
        else if(money == Money.dime)
            vm.deposit(.1);
        else if(money == Money.quarter)
            vm.deposit(.25);
        else if(money == Money.FUNF)
            vm.deposit(.5);
        else if(money == Money.EINEDOLL)
            vm.deposit(1);
        else if(money == Money.ZWEIDOLL)
            vm.deposit(2);
        else 
            System.out.println("Value nicht found");
    }

    protected static void selectItem()
    {

        
        System.out.println(vm.printIt());
        Scanner scan = new Scanner(System.in);
        // put your code here
        // This method reads the number provided using keyboard
        int num = scan.nextInt();

        while(num != 7)
        {
            if(num == 8)
            {
                vm.reset();
                break;
            }

            else
            {
                int prevNum = num;
                System.out.println("bist du kenn was du willst? - 9. Ja 10. Nein");
                scan = new Scanner(System.in);
                num = scan.nextInt();

                if(num == 9 )
                {
                    if(vm.buyIt(prevNum))
                    {
                        System.out.println(vm.getItem(prevNum));
                        
                    }
                }
                
            }
            System.out.println(vm.toString());
            System.out.println(vm.printIt());
            
            scan = new Scanner(System.in);
            num = scan.nextInt();
        }
        // Closing Scanner after the use
        scan.close();
        
        // Displaying the number 
        System.out.println("Auf Wiederzehen ");
    }

    /**
     * An example of a method - replace this comment with your own
     *
     * @param  y  a sample parameter for a method
     * @return    the sum of x and y
     */
    
}
