
/**
 * Write a description of class VendingMachine here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */



public class VendingMachine
{
    // instance variables - replace the example below with your own
    double value;

    int CANDY,SNACK,  NUTS, Coke,Pepsi, Soda;
   
    
    /**
     * Constructor for objects of class VendingMachine
     */
    protected VendingMachine()
    {
        // initialise instance variables
        CANDY = 10;
        SNACK = 50;
        NUTS = 90;
        Coke = 25;
        Pepsi = 35;
        Soda = 45;
    }

    protected boolean buyIt(int items)
    {
        if(items == 1)
        {
            if(value >= CANDY)
            {
                value -= CANDY;
                //System.out.println("Hier is candy");
                return true;
            }
            return false;
        }
        else if(items == 2)
        {
            if(value >= SNACK)
            {
                value -= SNACK;
                //System.out.println("Hier is SNACK");
                return true;
            }
            return false;
        }
        else if(items == 3)
        {
            if(value >= NUTS)
            {
                value -= NUTS;
                //System.out.println("Hier is NUTS");
                return true;
            }
            return false;
        }
        else if(items == 4)
        {
            if(value >= Coke)
            {
                value -= Coke;
                //System.out.println("Hier is Coke");
                return true;
            }
            return false;
        }
        else if(items == 5)
        {
            if(value >= Pepsi)
            {
                value -= Pepsi;
                //System.out.println("Hier is Pepsi");
                return true;
            }
            return false;
        }
        else if(items == 6)
        {
            if(value >= Soda)
            {
                value -= Soda;
                //System.out.println("Hier is Soda");
                return true;
            }
            return false;
        }
        else 
        {
            return false;
            //System.out.println("Value nicht found");
        }

    }

    protected String getItem(int items)
    {
        if(items == 1)
        {
            
                return ("Hier is candy");
                 //true;
           
        }
        else if(items == 2)
        {
            return ("Hier is SNACK");
        }
        else if(items == 3)
        {
           return ("Hier is NUTS");
                
        }
        else if(items == 4)
        {
            return ("Hier is Coke");
        }
        else if(items == 5)
        {
            return ("Hier is Pepsi");
        }
        else if(items == 6)
        {
           return ("Hier is Soda");
        }
        else 
        {
            return "n/a";
        }

    }
    
    protected void deposit(double money)
    {
        
        value += money;
        
    }
    /**
     * An example of a method - replace this comment with your own
     *
     * @param  y  a sample parameter for a method
     * @return    the sum of x and y
     */

    protected String printIt()
    {
        String s = "";
        s+= ("Bitte kauf eine sache von laden: ");
        s+= "\n";
        s+=("Habe 1. fur CANDY\t: " + CANDY + " ************ ");
        s+= "\n";
        s+=("Habe 2. fur SNACK\t: " + SNACK + " ************ ");
        s+= "\n";
        s+=("Habe 3. fur NUTS\t: " + NUTS + " ************ ");
        s+= "\n";
        s+=("Habe 4. fur Coke\t: " + Coke + " ************ ");
        s+= "\n";
        s+=("Habe 5. fur Pepsi\t: " + Pepsi + " ************ ");
        s+= "\n";
        //System.out.print("Habe 6. fur Soda\t: " + Soda + " ************ ");
        s+=("Habe 6. fur Soda\t: " + Soda + " ************ ");
        s+= "\n";
        s+=("Habe 7. fur nicht sache\t: " + " ************ ");
        s+= "\n";
        s+=("Habe 8. nicht alt (reset)\t: " + " ************ ");
        s+= "\n";

        return s;
    }
  

    //@Override
    public String toString()
    {
        return ("money = " + value + "\n");
    }

    protected void reset()
    {
        System.out.println("reset");
        value = 0;
    }
}
