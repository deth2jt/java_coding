
/**
 * Write a description of class PrimeOrNot here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
//https://www.geeksforgeeks.org/difference-between-abstract-class-and-interface-in-java/
import java.util.Set;
import java.util.List;
import java.util.ArrayList;
import java.util.LinkedHashSet;


public class TestFoo
{
    // instance variables - replace the example below with your own
    public static void main(String args[])
    {
        int[] anArray = {1,2,3,4,5,6,7};

        for(int x = 0; x < anArray.length; x++)
        {
            System.out.println("it is " +  isPrime(anArray[x]) + " that " + anArray[x] + " is a prime number");
            System.out.println("Fibonacci fur " +  anArray[x] + " is: " + fib(anArray[x]));
        }
        System.out.println("isPalindrome for 123321 is: " + isPalindromeString("12321") + " length: " + (isPalindromeInt(123321)));
        System.out.println("armstrongNum for 125 is: " + armstrongNum(153) );
        System.out.println("fact for 4 is: " + fact(4) );
        System.out.println("reverseString for BADONE is: " + reverseString("BADONE") );
        removeDupSet();
        patternPrint();
        System.out.println("removing duplicate from string for BAOBEDONE is: " + removeDup("BAOBEDONE") );
        System.out.println("gcd of 24 16 is: " + gcd(24,16) );
        System.out.println("gcd of 24 16 is: " + gcd2(24,16) );
        System.out.println("lowerSqrt of 10 is: " + lowerSqrt(10) );
        int[] s =  {1,2,3,4,5};
        System.out.println("binarySearch of of 12345: " + binarySearch(s, 5) );
        System.out.println("Reverse array in place of 12345: " + getArray(reverseInPlace(s)   )     );
        System.out.println("leapYear of 2000 is: " + leapYear(2000) );
        System.out.println("stringAnagram  is: " + stringAnagram("sat","ats") );
        
    }

    public static boolean stringAnagram(String str1,String str2)
    {
  
        //else if(str2.indexOf(str1.charAt(0)) != -1 )
        //    true and str2.replace(oldChar, newChar).charAt(0)
        int count = 0;
        while(count < str1.length())
        {
            str2 = str2.replace(str1.charAt(count), ' ');
            count++;   
        }
        //System.out.println("str1  is: " + str1 + " str2  is: " + str2 );
        if(str2.equals( createString( ' ', str1.length() )    ) )
            return true;
        else
            return false;    
    }

    public static String createString(char char1, int val)
    {
        //int count = 0;
        String s = "";
        for(int x = 0; x < val; x++)
        {
            s += char1;
        }
        return s;
    }
    /*
        Asummes ordered list
    */
    public static int binarySearch(int[] anArray, int val)
    {
        int returnVal = -1;
        int i = (int) Math.ceil(anArray.length / 2);
        while(i > 0 && i <= anArray.length)
        {
            if(val < anArray[i-1] )
                i = (int) Math.ceil(i /2.0);
            else if(val > anArray[i-1] )
                i = (int) Math.ceil(3*i /2.0);
            else
            {
                returnVal = anArray[i-1];
                i++;
            }
            //System.out.println("iiiiii: " + i + " anArray[i-1]: " + anArray[i-1] + " array: " + getArray(anArray));
        }
        return returnVal;
    }

    public static boolean leapYear(int year)
    {
        if(year % 100 == 0)
            if(year % 400 == 0)
                return true;
            else    
                return false;
        else if(year % 4 == 0 )
            return true;
        else
            return false;
        //return (year == 100 && year == 400) || year 
    }

    public static int[] reverseInPlace(int[] args)
    {
        int countForward = 0;
        int countBackward = args.length;
        while(countForward <= countBackward)
        {
            countBackward--;
            int temp =args[countForward];
            
            args[countForward] = args[countBackward];
            args[countBackward] = temp;

            countForward++;
        }
        return args;
    }

    public static String getArray(int[] args)
    {
        String s = "";
        for(int x = 0; x < args.length; x++)
        {
            s += args[x];
            s += " ";
        }
        return s;
    }


    public static int lowerSqrt(int val)
    {
        if(val <= 0)
        {
            return val;
        }
        
        int i = 1;

        while(i * i <= val)
        {
            i++;
        }

        return  i;

    }

    public static int gcd2(int val1, int val2)
    {
        if(val1 == val2)
            return val1;
        else if(val1 > val2)
            return gcd2(val1 - val2, val2);
        else
            return gcd2(val1, val2- val1);
    }   


    public static int gcd(int val1, int val2)
    {
        int returnVal = 1;
        for(int x = 1; x <= val1 &&  x <= val2; x++)
        {
            if(val1 % x == 0 && val2 % x == 0)
                returnVal = x;
        }
        return returnVal;
    }


    public static String removeDup(String strValue)
    {
        //String str = strValue;
        for(int x = 0; x < strValue.length(); x++)
        {
            int count = 1;
            while(count + x < strValue.length())
            {
                if(strValue.charAt( x) == strValue.charAt(count+x) &&  strValue.charAt( count+x) != ' ')
                    strValue = strValue.substring(0, count+x) + " " +  strValue.substring(count+x+ 1, strValue.length());
                count++;
            }
        }

        return strValue;
    }


    public static void patternPrint()
    {
        int len = 5;
        for(int x = 0; x < len; x++)
        {
            System.out.print(" ");
           
        }
        System.out.println("");
        for(int x = 0; x < len; x++)
        {
            System.out.print("*");
           
        }
        System.out.println("");
    }

    public static void removeDupSet()
    {
        List<Character> chars = new ArrayList<Character>();

        chars.add('d');
        chars.add('r');
        chars.add('j');
        chars.add('i');
        chars.add('o');
        chars.add('q');
        chars.add('j');
        chars.add('d');

        System.out.println("chars is: " + chars );
        
        Set<Character> char2 = new LinkedHashSet<Character>();
        char2.addAll(chars);
        System.out.println("char2 is: " + char2 );
        chars = new ArrayList<Character>();
        chars.addAll(char2);
        System.out.println("chars is: " + chars );

    }


    public static String reverseString(String val)
    {
        int len = val.length() ;
        //char[] str = new char[len];
        String str = "";
        for(int x = len; x > 0; x--)
        {
            str +=   val.substring(x-1, x);
        }
        return str;
    }


    public static int fact(int intVal)
    {
        if(intVal <= 1)
            return intVal;
        return intVal * fact(intVal - 1);
    }


    public static boolean isPrime(int val)
    {
        return val % 2 != 0;

    }


    static int fib(int n) 
    { 
        if (n <= 1) 
            return n; 
        return fib(n-1) + fib(n-2); 
    } 

    public static int[] lengthOfInt(int intValue)
    {
        
        int length = 0;
        int offset = 1;
        while(offset <= intValue)
        {
            length++;
            offset *= 10;
        }
        int[] values = {length, offset};
        return values;
    }

    public static boolean  isPalindromeInt(int intValue)
    {
        //int length = lengthOfInt(intValue)[0];
        //return length;
        boolean boolVal = true;
        int offset = lengthOfInt(intValue)[1];
        int lowerOffset = 10;

        while(offset >=  lowerOffset )
        {
            offset /= 10;
            int div = (lowerOffset/10);
            int lower =  (intValue % lowerOffset)/div;
            int upper = (intValue / offset)  % 10;
            System.out.println("lower: " + lower + " upper " + upper);
            boolVal = boolVal && (lower ==   upper);

            lowerOffset *= 10;
            
        }
        return boolVal;
    }

    static public boolean isPalindromeString(String str)
    {
        boolean val = true;
        for(int x = 0; x < str.length()/2; x++)
        {
            val = val && (str.charAt(x) == str.charAt( str.length()- (x+ 1)) );
        }
        return val;
    }

    public static boolean armstrongNum(int intValue)
    {
        int length = lengthOfInt(intValue)[0];
        int sum = 0;
        int lowerOffset = 10;

        while(length > 0 )
        {
            
            int div = (lowerOffset/10);
            int lower =  (intValue % lowerOffset)/div;
            sum += (lower * lower * lower);
            //System.out.println("xxxxx: " + sum  + " lower: " + lower);
            lowerOffset *= 10;
            length--;
        }
        //System.out.println("sumsumsum: " + sum );
        return intValue == sum;
    }
}
